import React, {Component} from 'react';
import axios from 'axios';


class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
        count: 0,
        weather: 0,
    };

    this.API_KEY = "8c164618fc1ceed5adcef1e9faa86e04";
    this.getData = this.getData.bind(this);
  }

  componentDidMount() {
    document.addEventListener('click', () => {
      this.setState({
        count: this.state.count + 1
      });
    });
  }

  getData(){
      axios.get("https://api.openweathermap.org/data/2.5/weather?q=Chihuahua&units=metric&appid="+this.API_KEY)
          .then(response => {
              this.setState({
                 weather:response.data.main.temp
              });
          }).catch( error => {
              console.log(error)
      });
  }

  render() {
    return (
      <div>

          <button onClick={this.getData} className="hui-button">Weather in Chihuahua!</button>
          Count: {this.state.count}
          <br/>
          <h3>Weather:</h3>
          <p className="project-start-end text-medium">
              {
                 this.state.weather
              }
              &#8451;
          </p>
      </div>
    );
  }
}

export default App;
